function burst() {
    var burstPane = document.getElementById("burst-pane");
    var burstPictures = burstPane.getElementsByClassName("burst-pic");

    function takeAllPhotos(i) {
        if (i < burstPictures.length) {
            var imgLink = burstPictures[i].getElementsByTagName('a')[0];
            var imgEl = imgLink.childNodes[0];

            countdown(1, function () {
                capture(function(imgUrl) { imgEl.src = imgUrl['thumb']; imgLink.href = imgUrl['orig']; });
                takeAllPhotos(i+1);
            });
        }
    }

    takeAllPhotos(0);
}

function capture(cb) {
    var req = new XMLHttpRequest();
    req.open('GET', '/capture', true);
    req.onload = function() {
        msg = JSON.parse(req.responseText);
        console.log(msg);
        cb(msg);
    }
    req.send();
};

function spin(burstPictures) {
    [].forEach.call(burstPictures, function(burstPic) {
        var spinner = new Spinner().spin();
        burstPic.appendChild(spinner.el);
    });
};

function countdown(from, cb) {
    var timer = document.getElementById("timer");
    var drawFn = function() {
        if (from == 0) {
            window.clearInterval(intervalId);
            cb();
            from = "";
        }
        timer.innerHTML = from;
        from -= 1;
    };
    var intervalId = window.setInterval(drawFn, 1000);
}
