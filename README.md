# Wedding photobooth

A simple Python-based photobooth for our wedding.
Built as a webapp with a Python backend that uses gphoto2
to capture and download pictures from a camera over USB.

[Screenshot](https://bitbucket.org/pierrelux/wedding-photobooth/raw/master/screenshot.png)

[Pierre-Luc and Gayane](http://gayanepierreluc.com)
