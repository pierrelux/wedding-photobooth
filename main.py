import os
import sys
import subprocess
import gphoto2 as gp
from PIL import Image
from jinja2 import Template
from flask import Flask, render_template, Response, jsonify, make_response, send_from_directory

IMAGE_PATH = 'static/img/'

def make_thumbnail(path):
   im = Image.open(path)
   im.thumbnail((700,400))
   name = path.split('.')
   print name
   outname = name[0] + '-thumbnail.' + name[1]
   im.save(outname)
   return outname

def collate_photos():
    imgthumbs = [os.path.join(IMAGE_PATH, fname) for fname in os.listdir(IMAGE_PATH) if fname.endswith('-thumbnail.JPG')]
    imgorig = [name.split('-thumbnail.JPG')[0] + '.JPG' for name in imgthumbs]

    with open('static/gallery.html', 'w') as frdr, open('templates/gallery.html', 'r') as ftpl:
        tpl = Template(ftpl.read()).render(imgpaths=zip(imgthumbs,imgorig))
        frdr.write(tpl)

class Camera:
    def __init__(self):
        pass

    def open(self):
        self.context = gp.gp_context_new()
        self.camera = gp.check_result(gp.gp_camera_new())
        gp.check_result(gp.gp_camera_init(self.camera, self.context))

    def close(self):
        gp.check_result(gp.gp_camera_exit(self.camera, self.context))

    def capture(self, path='./'):
        file_path = gp.check_result(gp.gp_camera_capture(
            self.camera, gp.GP_CAPTURE_IMAGE, self.context))

        target = os.path.join(path, file_path.name)
        camera_file = gp.check_result(gp.gp_camera_file_get(
            self.camera, file_path.folder, file_path.name,
            gp.GP_FILE_TYPE_NORMAL, self.context))
        gp.check_result(gp.gp_file_save(camera_file, target))

        thumbname = make_thumbnail(target)
        collate_photos()

        return thumbname, target

cam = Camera()
app = Flask(__name__)

@app.route("/")
def main():
    return render_template('main.html')

@app.route("/gallery")
def gallery():
    return send_from_directory('static', 'gallery.html')

@app.route("/capture")
def capture():
    try:
        thumb, full = cam.capture('static/img')
    except gp.GPhoto2Error:
        return make_response(jsonify({'error': 'Camera error'}), 500)
    return jsonify({'thumb': thumb, 'orig': full})

if __name__ == "__main__":
    try:
        cam.open()
    except gp.GPhoto2Error as e:
        print 'It looks like the camera is unavailable.', e
        sys.exit(-1)

    app.run()
    cam.close()
